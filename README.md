### Prerequisites
Make sure all of list below is already installed on your mechine
1. Node JS version 14.x.x or latest
2. NPM version 7.x.x or latest
3. Mysql 8.x.x or latest

### Installation
1. Clone the repo
   ```sh
   git clone https://gitlab.com/swrdna/bookstore.git
   ```
2. Install NPM packages
   ```sh
   npm install
   ```
3. Create new mysql database and import sql dump file in this repo (db_bookstore_development.sql) to new database
   ```sh
   mysql -u username -p database_name < db_bookstore_development.sql
   ```
4. Create a `.env` file in the root directory of this repo. Copy and paste environment-specific variables below to your `.env` file
   ```sh
   DB_HOST=your_db_host
   DB_USERNAME=your_db_username
   DB_PASSWORD=your_db_password
   DB_NAME=your_db_name
   ```
5. Run this app using `npm start` command
6. Open your favorite browser and access to `localhost:5000`
7. For consuming API of this app, please read the API Documentation [here](https://documenter.getpostman.com/view/17090719/Tzz7Qe68)