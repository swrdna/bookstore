const express = require('express');
const app = express();
const port = process.env.PORT || 5000;

require('dotenv').config()

app.use(express.json());
app.use(express.urlencoded({
  extended: true
}));

app.get('/', (req, res) => {
  res.send("Hello World from Node JS App");
});

const db = require("./src/models");
db.sequelize.sync();

require("./src/routes/book.routes")(app);
require("./src/routes/genre.routes")(app);

app.listen(port, () => {
  console.log(`Server is listening on port ${port}`);
});
