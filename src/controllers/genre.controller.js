const db = require("../models");
const Genre = db.genres;
const Op = db.Sequelize.Op;

exports.create = (req, res) => {
  if (!req.body.name) {
    res.status(400).send({
      message: "Name can not be empty"
    });
    return;
  }

  const genreParams = {
    name: req.body.name
  };

  Genre.create(genreParams)
    .then(data => {
      res.send({
        message: "Genre was successfully created",
        data: data
      });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the genre"
      });
    });
};

exports.findAll = (req, res) => {
  const nameParams = req.query.name;
  const condition = nameParams ? { name: { [Op.like]: `%${nameParams}%` } } : null;

  Genre.findAll({ where: condition })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving genres"
      });
    });
};

exports.findOne = (req, res) => {
  const id = req.params.id;

  Genre.findByPk(id)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: `Error retrieving genre with id=${id}`
      });
    });
};

exports.update = (req, res) => {
  const id = req.params.id;

  Genre.update(req.body, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        Genre.findByPk(id)
          .then(data => {
            res.send({
              message: "Genre was successfully updated",
              data: data
            });
          });
      } else {
        res.send({
          message: `Cannot update genre with id=${id}. Maybe genre was not found or req.body is empty`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: `Error retrieving genre with id=${id}`
      });
    });
};

exports.delete = (req, res) => {
  const id = req.params.id;

  Genre.destroy({
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Genre was successfully deleted"
        });
      } else {
        res.send({
          message: `Cannot delete genre with id=${id}. Maybe genre was not found`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: `Error retrieving genre with id=${id}`
      });
    });
};

exports.deleteAll = (req, res) => {
  Genre.destroy({
    where: {},
    truncate: false
  })
    .then(nums => {
      res.send({ message: `${nums} genres were successfully deleted` });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all genres"
      });
    });
};
