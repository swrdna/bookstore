const db = require("../models");
const Book = db.books;
const Genre = db.genres;
const Op = db.Sequelize.Op;

exports.create = (req, res) => {
  if (!req.body.title) {
    res.status(400).send({
      message: "Title can not be empty"
    });
    return;
  }

  const bookParams = {
    title: req.body.title,
    genreId: req.body.genreId,
    isPublished: req.body.isPublished ? req.body.isPublished : true
  };

  Book.create(bookParams, {include: Genre})
    .then(book => {
      Book.findByPk(book.id, {include: Genre})
        .then(data => {
          res.send({
            message: "Book was successfully created",
            data: data
          });
        })
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Book"
      });
    });
};

exports.findAll = (req, res) => {
  const titleParams = req.query.title;
  const condition = titleParams ? { title: { [Op.like]: `%${titleParams}%` } } : null;

  Book.findAll({ where: condition, include: Genre })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving books"
      });
    });
};

exports.findOne = (req, res) => {
  const id = req.params.id;

  Book.findByPk(id, {include: Genre})
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: `Error retrieving book with id=${id}`
      });
    });
};

exports.update = (req, res) => {
  const id = req.params.id;

  Book.update(req.body, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        Book.findByPk(id, {include: Genre})
          .then(data => {
            res.send({
              message: "Book was successfully updated",
              data: data
            });
          });
      } else {
        res.send({
          message: `Cannot update book with id=${id}. Maybe book was not found or req.body is empty`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: `Error retrieving book with id=${id}`
      });
    });
};

exports.delete = (req, res) => {
  const id = req.params.id;

  Book.destroy({
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Book was successfully deleted"
        });
      } else {
        res.send({
          message: `Cannot delete book with id=${id}. Maybe book was not found`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: `Error retrieving book with id=${id}`
      });
    });
};

exports.deleteAll = (req, res) => {
  Book.destroy({
    where: {},
    truncate: false
  })
    .then(nums => {
      res.send({ message: `${nums} books were successfully deleted` });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all books."
      });
    });
};
