module.exports = (sequelize, Sequelize) => {
  const Genre = require("./genre.model.js")(sequelize, Sequelize);
  const Book = sequelize.define("book", {
    title: {
      type: Sequelize.STRING
    },
    genreId: {
      type: Sequelize.INTEGER
    },
    isPublished: {
      type: Sequelize.BOOLEAN
    }
  });

  Book.belongsTo(Genre)

  return Book;
};