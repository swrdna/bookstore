module.exports = app => {
  const books = require('../controllers/book.controller');
  let router = require("express").Router();

  // Create a new book
  router.post('/', books.create);
  
  // Show all books
  router.get('/', books.findAll);
  
  // Show a book with id
  router.get('/:id', books.findOne);
  
  // Update a book with id
  router.put('/:id', books.update);
  
  // Delete a book with id
  router.delete('/:id', books.delete);
  
  // Delete all books
  router.delete('/', books.deleteAll);

  app.use('/api/books', router);
}
