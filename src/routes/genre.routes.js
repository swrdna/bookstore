module.exports = app => {
  const genres = require('../controllers/genre.controller');
  let router = require("express").Router();

  // Create a new genre
  router.post('/', genres.create);
  
  // Show all genres
  router.get('/', genres.findAll);
  
  // Show a genre with id
  router.get('/:id', genres.findOne);
  
  // Update a genre with id
  router.put('/:id', genres.update);
  
  // Delete a genre with id
  router.delete('/:id', genres.delete);
  
  // Delete all genres
  router.delete('/', genres.deleteAll);

  app.use('/api/genres', router);
}
